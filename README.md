# Projeto mobile

# Lista de atividades

## Rafael

- [X] Finalizar engine do jogo

## Nós junto com pedagogia

- [X] Adicionar perguntas relevantes ao conteúdo
- [X] Adicionar imagens relacionadas às perguntas
- [ ] Alterar o ícone do projeto para utilização nas plataformas móveis

## Douglas

- [ ] Criar estilo para tela inicial do jogo
- [ ] Criar estilo para tela de perguntas do jogo
- [ ] Criar estilo para a tela final do jogo
