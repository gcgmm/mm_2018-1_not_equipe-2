import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

    state: any;
    score: any;
    questions: any;
    question: any;
    count: any;
    answers: any;

    constructor(public navCtrl: NavController) {
        this.state = "inicio";
        this.score = null;
        this.questions = [//
            {image:'assets/imgs/pictures/avião.jpg', answer:'assets/imgs/pictures/leão.jpg' },
            {image:'assets/imgs/pictures/leão.jpg', answer:'assets/imgs/pictures/avião.jpg' },
            {image:'assets/imgs/pictures/vaca.jpg', answer:'assets/imgs/pictures/faca.jpg' },
            {image:'assets/imgs/pictures/faca.jpg', answer:'assets/imgs/pictures/vaca.jpg' },
            {image:'assets/imgs/pictures/rato.jpg', answer:'assets/imgs/pictures/gato.jpg' },
            {image:'assets/imgs/pictures/gato.jpg', answer:'assets/imgs/pictures/rato.jpg' },
            {image:'assets/imgs/pictures/jarro.jpg', answer:'assets/imgs/pictures/carro.jpg' },
            {image:'assets/imgs/pictures/carro.jpg', answer:'assets/imgs/pictures/jarro.jpg' },
            {image:'assets/imgs/pictures/mamadeira.jpg', answer:'assets/imgs/pictures/cadeira.jpg' },
            {image:'assets/imgs/pictures/cadeira.jpg', answer:'assets/imgs/pictures/mamadeira.jpg' },
            {image:'assets/imgs/pictures/bolboleta.jpg', answer:'assets/imgs/pictures/chupeta.jpg' },
            {image:'assets/imgs/pictures/chupeta.jpg', answer:'assets/imgs/pictures/bolboleta.jpg' },
            {image:'assets/imgs/pictures/dinheiro.jpg', answer:'assets/imgs/pictures/brigadeiro.jpg' },
            {image:'assets/imgs/pictures/brigadeiro.jpg', answer:'assets/imgs/pictures/dinheiro.jpg' },
            {image:'assets/imgs/pictures/garrafa.jpg', answer:'assets/imgs/pictures/girafa.jpg' },
            {image:'assets/imgs/pictures/girafa.jpg', answer:'assets/imgs/pictures/garrafa.jpg' },
            {image:'assets/imgs/pictures/abelha.jpg', answer:'assets/imgs/pictures/ovelha.png' },
            {image:'assets/imgs/pictures/ovelha.png', answer:'assets/imgs/pictures/abelha.jpg' },
            {image:'assets/imgs/pictures/tesoura.jpg', answer:'assets/imgs/pictures/vassoura.jpg' },
            {image:'assets/imgs/pictures/vassoura.jpg', answer:'assets/imgs/pictures/tesoura.jpg' },
            {image:'assets/imgs/pictures/Presente.jpg', answer:'assets/imgs/pictures/dente.jpg' },
            {image:'assets/imgs/pictures/dente.jpg', answer:'assets/imgs/pictures/Presente.jpg' },
            {image:'assets/imgs/pictures/buzina.jpg', answer:'assets/imgs/pictures/piscina.jpg' },
            {image:'assets/imgs/pictures/piscina.jpg', answer:'assets/imgs/pictures/buzina.jpg' },
            {image:'assets/imgs/pictures/luva.jpg', answer:'assets/imgs/pictures/chuva.jpg' },
            {image:'assets/imgs/pictures/chuva.jpg', answer:'assets/imgs/pictures/luva.jpg' },
            {image:'assets/imgs/pictures/janela.jpg', answer:'assets/imgs/pictures/panela.jpg' },
            {image:'assets/imgs/pictures/panela.jpg', answer:'assets/imgs/pictures/janela.jpg' },
            {image:'assets/imgs/pictures/pincel.jpg', answer:'assets/imgs/pictures/anel.jpg' },
            {image:'assets/imgs/pictures/anel.jpg', answer:'assets/imgs/pictures/pincel.jpg' },
            {image:'assets/imgs/pictures/mola.jpg', answer:'assets/imgs/pictures/bola.jpg' },
            {image:'assets/imgs/pictures/bolo.jpg', answer:'assets/imgs/pictures/bolo.jpg' },
            {image:'assets/imgs/pictures/palhaço.jpg', answer:'assets/imgs/pictures/laço.jpg' },
            {image:'assets/imgs/pictures/laço.jpg', answer:'assets/imgs/pictures/palhaço.jpg' },
            {image:'assets/imgs/pictures/meia.jpg', answer:'assets/imgs/pictures/teia.jpg' },
            {image:'assets/imgs/pictures/teia.jpg', answer:'assets/imgs/pictures/meia.jpg' }
            //
        ];
        //let items = this.questions.map(q => q.answer);
        //console.log(Array.from(new Set(items)));
  }

    newQuestion(){
        this.question = this.questions[Math.floor(Math.random() * this.questions.length)];
        this.answers = [this.question.answer];
        //let items = this.questions.map(q => q.answer);
        while(this.answers.length < 3){
            let answer = this.questions[Math.floor(Math.random() * this.questions.length)].answer;
            if(this.answers.indexOf(answer) === -1){
                this.answers.push(answer);
            }
        }
        for(let i=0; i< 4; i++){
            let tmpPos1 = Math.floor(Math.random() * this.answers.length);
            let tmpPos2 = Math.floor(Math.random() * this.answers.length);

            let tmp = this.answers[tmpPos1];
            this.answers[tmpPos1] = this.answers[tmpPos2];
            this.answers[tmpPos2] = tmp;
        }
    }

    finishGame(selected: any){
        if(selected == undefined || selected == null){
            if(this.state === 'jogo'){
                this.state = 'inicio';
            }
            this.question = null;
        }else{
            if(selected == this.question.answer){
                this.score++;
            }

            if(this.count === 10){
                if(this.state === 'jogo'){
                    this.state = 'inicio';
                }
                this.question = null;
            }else{
                this.newQuestion();
            }

            this.count++;
        }
    }
    startGame(){
        this.newQuestion();
        this.count = 0;
        this.score = 0;
        if(this.state === 'inicio'){
            this.state = 'jogo';
        }
    }

}
